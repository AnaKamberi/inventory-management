package dto;

import jakarta.persistence.Entity;
import lombok.Data;

@Data

@Entity(name = "product_request")
public class ProductRequest {
    private String title;
    private String description;
    private Integer quantity;
    private Double price;
    private Integer categoryId;

//    @Override
//    public String toString() {
//        return "ProductRequest{" +
//                "title='" + title + '\'' +
//                ", description='" + description + '\'' +
//                ", quantity=" + quantity +
//                ", price=" + price +
//                ", categoryId=" + categoryId +
//                '}';
    }

