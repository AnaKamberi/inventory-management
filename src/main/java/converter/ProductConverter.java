package converter;

import dto.ProductRequest;
import entity.Category;
import entity.Product;

public class ProductConverter {
    public static Product convertRequestToEntity (ProductRequest request) {
        Product product = new Product();

        product.setCategory(new Category(request.getCategoryId()));
        product.setDescritpion(request.getDescription());
        product.setQuantity(request.getQuantity());
        product.setTitle(request.getTitle());
        product.setPrice(request.getPrice());
        return product;
    }
}
