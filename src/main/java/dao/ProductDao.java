package dao;

import entity.Product;

import java.util.List;

public interface ProductDao {
     void save(Product product);
    Product findBy(Long id);
    List<Product> findAll();
}
