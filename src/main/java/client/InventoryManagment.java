package client;

import dto.ProductRequest;
import service.ProductService;
import service.ProductServiceImpl;

import java.util.Scanner;

public class InventoryManagment {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Inventory Managment System");
        System.out.println("1. Add product");
        System.out.println("2. Display all products");
        System.out.println("3. Notify for low stock product");
        System.out.println("4. Add category");
        System.out.println("5. display all categories");

        int choice = sc.nextInt();

        switch (choice){
            case 1:
                createProduct(sc);
                break;
            case 2:
                displayAllProduct();
                break;
            case 3:
                //
                break;
            case 4:
                //
                break;

        }


    }

    private static void displayAllProduct() {
    }

    public static void createProduct(Scanner scanner){
        ProductRequest productRequest = new ProductRequest();
        System.out.println("Set product");
        productRequest.setTitle(scanner.next());
        System.out.println("Set description");
        productRequest.setDescription(scanner.next());
        System.out.println("Set quantity");
        productRequest.setQuantity(scanner.nextInt());
        System.out.println("Set category");
        productRequest.setCategoryId(scanner.nextInt());
        System.out.println("Set price");
        productRequest.setPrice(scanner.nextDouble());

        ProductService productService = new ProductServiceImpl();
        productService.createProduct(productRequest);


    }
}
